class HumanPlayer
  attr_accessor :mark, :name, :board

  def initialize(player_name)
    @name = player_name
  end

  def display(board_state)
    @board = board_state
    @board.display_state
  end

  def get_move
    input = []
    until valid_move?(input)
      puts 'Where will you place your mark?'
      puts 'Use the format row, column. Ex: the top-left square is 0, 0'
      input = gets.chomp.split(',').map(&:to_i)
      puts 'Invalid move' unless valid_move?(input)
    end
    input
  end
end

private

def valid_move?(pos)
  return @board.empty?(pos) unless pos.empty?
end
