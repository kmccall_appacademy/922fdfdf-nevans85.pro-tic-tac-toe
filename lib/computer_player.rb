class ComputerPlayer
  attr_accessor :mark, :name, :board

  def initialize(player_name)
    @name = player_name
  end

  def display(board_state)
    @board = board_state
  end

  def get_move
    return rand_move if winning_move.nil? && blocking_move.nil?
    return winning_move if blocking_move.nil?
    blocking_move
  end

  private

  LINES = [[[0, 0], [0, 1], [0, 2]],
           [[0, 0], [1, 1], [2, 2]],
           [[0, 0], [1, 0], [2, 0]],
           [[0, 1], [1, 1], [2, 1]],
           [[0, 2], [1, 1], [2, 0]],
           [[0, 2], [1, 2], [2, 2]],
           [[1, 0], [1, 1], [1, 2]],
           [[2, 0], [2, 1], [2, 2]]].freeze

  def winning_move
    LINES.each do |line|
      if can_win?(line, @mark)
        return line.select { |pos| @board.empty?(pos) }.flatten
      end
    end
    nil
  end

  def blocking_move
    opp_mark = :O if @mark == :X
    opp_mark = :X if @mark == :O
    LINES.each do |line|
      if can_win?(line, opp_mark)
        return line.select { |pos| @board.empty?(pos) }.flatten
      end
    end
    nil
  end

  def rand_move
    random_move = [nil, nil]
    random_move = [rand(0..2), rand(0..2)] until valid_move?(random_move)
    random_move
  end

  def valid_move?(pos)
    return @board.empty?(pos) unless pos == [nil, nil]
  end

  def can_win?(line, mark)
    line.count { |pos| @board.value_at(pos) == mark } == 2 &&
      line.count { |pos| @board.empty?(pos) } == 1
  end
end
