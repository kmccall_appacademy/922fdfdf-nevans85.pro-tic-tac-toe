class Board
  attr_accessor :grid

  def initialize(new_grid = [[nil, nil, nil],
                             [nil, nil, nil],
                             [nil, nil, nil]])
    @grid = new_grid
  end

  def place_mark(position, mark)
    @grid[position[0]][position[1]] = mark
  end

  def empty?(position)
    @grid[position[0]][position[1]].nil?
  end

  def value_at(position)
    @grid[position[0]][position[1]]
  end

  def winner
    win_mark = nil
    @grid.each do |row|
      win_mark = row[0] if !row[0].nil? && row.all? { |sq| sq == row[0] }
    end
    @grid[0].each_with_index do |mark, idx|
      win_mark = mark if (@grid[1][idx] == mark && @grid[2][idx] == mark) ||
                         (@grid[1][1] == mark && @grid[2][idx * -1 - 1] == mark)
    end
    win_mark
  end

  def over?
    return true unless winner.nil?
    return true if @grid.flatten.all? { |e| !e.nil? }
  end

  def display_state
    @grid.each do |row|
      row.each_with_index do |mark, idx|
        print mark.to_s
        print ' ' if mark.nil?
        print '/' unless idx == 2
      end
      puts ''
    end
  end
end
