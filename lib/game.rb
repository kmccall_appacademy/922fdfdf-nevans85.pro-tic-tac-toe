require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_accessor :board, :current_player
  attr_reader :p1, :p2

  def initialize(player_one, player_two)
    @board = Board.new
    @p1 = player_one
    @p2 = player_two
    @current_player = @p1
    @p1.mark = :O
    @p2.mark = :X
  end

  def switch_players!
    switch_to = nil
    switch_to = @p2 if @current_player == @p1
    switch_to = @p1 if @current_player == @p2
    @current_player = switch_to
  end

  def play_turn
    @current_player.display(@board)
    @board.place_mark(@current_player.get_move, @current_player.mark)
    switch_players!
  end

  def play
    play_turn until @board.over?
    @board.display_state
    switch_players!
    puts "#{@board.winner} is the winner." unless @board.winner.nil?
    puts "It is a tie. Cat's game" if @board.winner.nil?
  end
end

if $PROGRAM_NAME == __FILE__
  player_one = HumanPlayer.new('P1')
  player_two = ComputerPlayer.new('P2')
  game = Game.new(player_one, player_two)
  game.play
end
